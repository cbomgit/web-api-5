var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
app.set('trust proxy', true);

var baseUrl = 'https://apibaas-trial.apigee.net/cbomMoviesWebAPI4/sandbox';

/* GET all movies */
app.get('/movies', function(req, res, next) {

    
    //first get all movies
    request(baseUrl + '/movies', function(error, response, body){

        var movies = JSON.parse(body).entities;
        //if query asks for reviews, fetch all reviews
        if(req.query['reviews'] != null && req.query['reviews'] == 'true') {
            
            request(baseUrl + '/reviews', function(error, response, body){

                var reviews = JSON.parse(body).entities;
                var movieData = [];

                //iterate through each movie and get the corresponding review 
                for(var i = 0; i < movies.length; i++) {

                    var movie = movies[i];
                    var movieReview = reviews.filter(function(review) {
                        return review.movie == movie.uuid;
                    })

                     
                    movieData.push({
                        "movie": movie,
                        "reviews":movieReview
                    });
                }

                res.json({'status':'Ok', 'count':movies.length, 'movies':movieData});
            });
        }
        else {

            res.json({'status':'Ok', 'count':movies.length, 'movies':movies});   

        }
    });
});

/* Get a single movie based on title */
app.get('/movies/:movieTitle', function(req, res, next) {


    request(baseUrl + '/movies/' + req.params['movieTitle'], function(error, outerResponse, moviesBody) {

        if(outerResponse.statusCode != 200 || JSON.parse(moviesBody).entities.length == 0){
            res.status(outerResponse.statusCode);
            res.json({
                'error':{
                    'status':outerResponse.statusCode,
                    'message':'Movie not found'
                }
            })
        }
        else {

            if(req.query['reviews'] != null && req.query['reviews'] == 'true') {

                console.log("You wanted reviews");

                var queryStr = "?ql=movie='"+req.params['movieTitle']+"'";
                request(baseUrl + '/reviews' + queryStr, function(error, response, body) {

                    var entities = JSON.parse(body).entities;
                    var movies = JSON.parse(moviesBody).entities;
                    res.json({'status':'Ok', 'count':movies.length, 'movies':movies, 'reviews': entities});
                    
                });
            }
            else {

                console.log("You didn't ask for reviews");
                var entities = JSON.parse(moviesBody).entities;
                res.json({'status':'Ok', 'count':entities.length, 'movies':entities});

            }
        }
    });

});

/*Create a new movie*/
app.post('/movies', function(req, res, next) {

    if(req.body['title'] == null ){
        res.status(400);
        res.json({
            'error':{
                'status':400,
                'message':'The title is missing'
            }
        });
    }
    else if(req.body['date_released'] == null ){
        res.status(400);
        res.json({
            'error':{
                'status':400,
                'message':'Date released is missing'
            }
        });
    }
    else if(req.body['actors'] == null) {
        res.status(400);
        res.json({
            'error': {
                'status':400,
                'message':'Actors array is required'
            }
        });
    }
    else if(req.body['actors'].length != 3) {
        res.status(400);
        res.json({
            'error': {
                'status':400,
                'message':'Provide at least 3 actors'
            }
        });
    }
    else {
        var options = {
            uri: baseUrl + '/movies',
            method:'POST',
            json: {
                "name":req.body['title'],
                "title":req.body['title'],
                "date_released":req.body['date_released'],
                "actors":req.body['actors']
            }
        };

        request(options, function(error, response, body) {
            res.json({'status':'Ok', 'movies':body.entities});
        });
    }
});

/*Delete a movie by name*/
app.delete('/movies/:name', function(req, res, next) {
    var options = {
        uri:baseUrl + '/movies/'+req.params['name'],
        method:'DELETE'
    };

    request(options, function(error, response, body) {
        if(!JSON.parse(body).entities){
            res.status(404);
            res.json({
                'error':{
                    'status':404,
                    'message':'Movie not found'
                }
            })
        }else {
            res.json({'status':'Ok', 'movies':JSON.parse(body).entities});
        }
    });
});

/********** Routes for reviews  *************/


//get all reviews
app.get('/reviews', function(req, res, next) {

    request(baseUrl + '/reviews', function(error, response, body) {

        var entities = JSON.parse(body).entities;
        res.json({'status':'Ok', 'count':entities.length, 'movies':entities});    
    });

})


//get reviews for a particular movie
app.get('/reviews/:movieTitle', function(req, res, next) {

    var queryStr = "?ql=movie='"+req.params['movieTitle']+"'";
    request(baseUrl + '/reviews' + queryStr, function(error, response, body) {
        if(JSON.parse(body).entities.length == 0){
            res.status(404);
            res.json({
                'error':{
                    'status':404,
                    'message':'No reviews found for ' + req.params['movieTitle']
                }
            })
        }else {
            var entities = JSON.parse(body).entities;
            res.json({'status':'Ok', 'count':entities.length, 'reviews':entities});
        }
    });
});

//create a review
app.post('/reviews', function(req, res, next) {


    //validate the request data    
    if(req.body['movie'] == null ){
        res.status(400);
        res.json({
            'error':{
                'status':400,
                'message':'Need to know which movie this review is for'
            }
        });
    }
    else if(req.body['reviewer'] == null ){
        res.status(400);
        res.json({
            'error':{
                'status':400,
                'message':'Reviewer is missing'
            }
        });
    }
    else if(req.body['review'] == null) {
        res.status(400);
        res.json({
            'error': {
                'status':400,
                'message':'Review content is missing (review parameter)'
            }
        });
    }
    else if(req.body['stars'] == null) {
        res.status(400);
        res.json({
            'error': {
                'status':400,
                'message':'Stars rating is missing'
            }
        });
    }
    else {

        request(baseUrl + '/movies/'+req.body['movie'], function(error, response, body) {

            //make sure a valid movie was passed in
            if(response.statusCode == 404) {
                res.status(400);
                res.json({
                    "error": {
                        'status':400,
                        'message': "Couldn't find that movie"
                    }
                })
            }
            else {
                //create the review and set its movie property to the passed in UUID
                var options = {
                    uri:baseUrl + '/reviews',
                    method:'POST',
                    json: {
                        "movie":req.body['movie'],
                        "reviewer":req.body['reviewer'],
                        "review":req.body['review'],
                        "stars":req.body['stars']
                    }
                };

                request(options, function(error, response, body) {

                    res.json({"status":"Ok","review": body.entities});
                
                });
            }
        });
    }
});

//delete a review
app.delete('/reviews/:reviewUUID', function(req, res,next) {

    var options = {
        uri: baseUrl + '/reviews/'+req.params['reviewUUID'],
        method:'DELETE'
    };

    request(options, function(error, response, body) {
        if(!JSON.parse(body).entities){
            res.status(404);
            res.json({
                'error':{
                    'status':404,
                    'message':'Review not found'
                }
            })
        }else {
            res.json({'status':'Ok', 'review':JSON.parse(body).entities});
        }
    });

})

app.listen(3000, function() {
   console.log('Listening on 3000!')
});