To install Node app: 

$ git clone <repo url>
$ npm install
$ node index.js

Base URL: 
https://webapicbom-test.apigee.net/webapi5

Application routes: 

GET /movies - retrieves all movies
   *optional param ?reviews=true will include reviews with each movie result

GET /movies/<movie name> - retrieves movies matching name in parameter
   * optional param ?reviews=true will include movie reviews with response

POST /movies - creates a new movie, the following body is expected: 

   title - title of the movie
   date_released - release date of movie
   actors - array of length 3 for actors in each movie

DELETE /movies/<movie name> delete the specified movie


GET /reviews - retrieves all reviews for all movies


GET /reviews/<movie uuid> - retrieves the reviews for a specific movie
   * /movies/<uuid or name>/reviews would have been ideal, but apigee's entity
     connetion wasn't working for me

POST /reviews - create a new review and tie it to a movie

   request body = { 
      movie: <movie uuid> - returns 400 if movie is not found
      reviewer: name of the reviewer
      review: review content
      stars: rating
   }

DELETE /reviews/<review uuid> - deletes the review with <review uuid>
